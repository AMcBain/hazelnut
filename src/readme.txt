Hazelnut  http://hazelnut.asmcbain.net

SUMMARY:

Hazelnut is an IRC bot written in JavaScript using Java via Rhino to get the necessary classes
to connect to IRC. Hazelnut offers a plugin based model for maximum flexibility in features as
well as a small API to help in doing other functions not directly related to IRC events.

REQUIREMENTS:

Java must be installed on the machine the bot is to be run on. Minimum Java version
is limited by Rhino, so it is probably Java 5.0 or higher, though 6.0 is recommended.


LICENSE:

Redistribution and use, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above, this list of conditions, and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
   following disclaimer in the documentation and/or other materials provided with the distribution.
3. The name of the author may not be used to endorse or promote products derived from this software without
   specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.


LICENSE EXCEPTIONS:

The PircBot mod in the lib directory and Mozilla's Rhino js.jar are merely packaged with this framework and are
subject to their own licenses which are included alongside their files in that directory.

Non-core plugins not included with Hazelnut releases are not bound by the same license as Hazelnut. This includes
all "plugins" listed in the SVN repository as well as those not hosted by the Hazelnut website or the Bitbucket
project pages for Hazelnut. By default they are copyright by the author of the plugin. Plugin authors may
choose to license their plugins, these licenses should be displayed at the top of the plugin.

The authors of and contributors to Hazelnut are not responsible for the content of these plugins, they are to
be used at one's own risk.
