/*global java: false, HazelnutFramework: false, HazelnutPlugin: false */
/**
 * Creates an SynchronousEventTask object with the given {@link HazelnutFramework}, unique id, and {@link EventType}.
 *
 * @class
 * This class is designed to aid in turning normally asynchronous event-related
 * requests into synchronous ones.
 * <br><br>
 * A simple (and silly) use-case could be as follows:
 * <pre>
 * var framework = ...
 * var task = new SynchronousEventTask(framework, "get-join-channel", EventType.JOIN);
 * task.onEvent = function(framework, event) {
 * 	event.handle();
 * 	return event.channel;
 * };
 * framework.joinChannel("#joinchantest");
 * print(task.run());
 * </pre>
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 * @param {HazelnutFramework} framework The framework on which this task will be run.
 * @param {String} id An identifier for this lock.
 * @param {EventType} type The type of event on which to run this task.
 * @throws an error if the supplied framework object is not an instanceof {@link HazelnutFramework}
 */
/*
 * <br>
 * Another test case (which doesn't require an IRC connection) can be performed as follows:
 * <pre>
 * var task = new AsynchronousTask(framework, "test");
 * var thread = new java.lang.Thread(new java.lang.Runnable() {
 * 	run: function() {
 * 		print(task.run());
 * 	}
 * });
 * thread.start();
 * task.onEvent(framework, new Event());
 * task.set("foo");
 * </pre>
 */
function SynchronousEventTask(framework, id, type) {
	if(!(framework instanceof HazelnutFramework)) {
		throw "SynchronousEventTask#constructor: framework must be an instance of HazelnutFramework";
	}
	if(!(type instanceof EventType)) {
		throw "SynchronousEventTask#constructor: type must be an instance of EventType";
	}

	var $this = this;

	/**
	 * Unique ID for this AsynchronousTask.
	 *
	 * @readonly
	 * @type String
	 */
	this.id = null;
	this.__defineGetter__("id", function() { return id; });

	/**
	 * Called when this task needs to process some information, override as necessary.
	 * The processing code should mark the event handled when the data is finally processed
	 * and should return the data from function.
	 *
	 * @param {HazelnutFramework} framework The HazelnutFramework instance that generated this event.
	 * @param {Event} event The event object generated for this IRC event.
	 * @returns processed data object
	 * @type Object
	 */
	this.onEvent = function(framework, event) {
		event.handle();
	};

	var synchq = new java.util.concurrent.SynchronousQueue();

	// Yar, we gotta use a plugin ... mighty clunky, but best way to do it.
	// This plugin is added and removed before and after use, respectively.
	var handler = new HazelnutPlugin();
	handler.family = "asynchronoustask";
	handler.name = id;
	handler.onEvent = function(framework, event) {
		if($this.onEvent) {
			var results = $this.onEvent(framework, event);
			if(event.handled) {
				framework.removeEventHandler(type, handler);
				synchq.put(results);
			}
		}
	};

	/**
	 * Sets the value to be returned from this task, waiting (if necessary) until a {@link #run} is called (note: 
	 * this will not need to be called under most circumstances).
	 */
	this.set = function(value) {
		synchq.put(value);
	};

	/**
	 * Runs this task.
	 *
	 * @returns the data this task was supposed to get
	 * @type Object
	 */
	this.run = function() {
		framework.addEventHandler(type, handler);
		var value = synchq.take();
		return value;
	};


	this.toString = function() {
		return "[object AsynchronousTask]";
	};

}
