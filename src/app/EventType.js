/**
 * New EventTypes cannot be instantiated.
 *
 * @class
 * A class containing all the types of events.
 *
 * @author A.McBain, 2009
 * @since 1.0.0
 * @constructor
 * @param {String} value The event name
 * @throws an error if an attempt is made to instantiate this class
 */
var EventType = (function() {
	var restrict = false;

	function EType(name) {
		if(restrict) {
			throw "EventType#constructor: unsupported operation";
		}
		this.__unwrap__ = true;

		/**
		 * The name of the EventType.
		 *
		 * @readonly
		 */
		this.name = null;
		this.__defineGetter__("name", function() { return name; });

		this.toString = function() {
			return "[object EventType(" + name + ")]";
		};
	}
	EType.restrict = function() { restrict = true; };

	return EType;
}());

/* Custom Events, for the Hazelnut framework */

/**
 * Someone requested help information
 * @since 1.0.0
 */
EventType.HELP = new EventType("HELP");
/**
 * Someone requested a list of commands
 * @since 1.0.0
 */
EventType.COMMAND = new EventType("COMMAND");


/* IRC events, added by us */

/**
 * Somone sets a channel's topic. This is to differentiate between a
 * topic that already exists ({@link EventType.TOPIC}) and a newly set topic.
 * @since 1.0.0
 */
EventType.SET_TOPIC = new EventType("SET_TOPIC");


/* IRC events, defined by PircBot */

/**
 * Someone does an action
 * @since 1.0.0
 */
EventType.ACTION = new EventType("ACTION");
/**
 * A connection notice is received
 * @since 1.0.0
 */
EventType.CONNECT = new EventType("CONNECT");
/**
 * Someone is deoped
 * @since 1.0.0
 */
EventType.DEOP = new EventType("DEOP");
/**
 * Someone is devoiced
 * @since 1.0.0
 */
EventType.DE_VOICE = new EventType("DE_VOICE");
/**
 * A disconnection notice is recieved
 * @since 1.0.0
 */
EventType.DISCONNECT = new EventType("DISCONNECT");
/**
 * A CTCP FINGER command is received
 * @since 1.0.0
 */
EventType.FINGER = new EventType("FINGER");
/**
 * A invitation to join another channel is received
 * @since 1.0.0
 */
EventType.INVITE = new EventType("INVITE");
/**
 * Someone joins a channel
 * @since 1.0.0
 */
EventType.JOIN = new EventType("JOIN");
/**
 * Someone is kicked
 * @since 1.0.0
 */
EventType.KICK = new EventType("KICK");
/**
 * Someone says something on a channel
 * @since 1.0.0
 */
EventType.MESSAGE = new EventType("MESSAGE");
/**
 * A channel mode is changed
 * @since 1.0.0
 */
EventType.MODE = new EventType("MODE");
/**
 * Someone changes their nick
 * @since 1.0.0
 */
EventType.NICK_CHANGE = new EventType("NICK_CHANGE");
/**
 * Someone sends a notice to us
 * @since 1.0.0
 */
EventType.NOTICE = new EventType("NOTICE");
/**
 * Someone is opped
 * @since 1.0.0
 */
EventType.OP = new EventType("OP");
/**
 * Someone parts a channel
 * @since 1.0.0
 */
EventType.PART = new EventType("PART");
/**
 * Someone pings us
 * @since 1.0.0
 */
EventType.PING = new EventType("PING");
/**
 * Someone sends a private message to us
 * @since 1.0.0
 */
EventType.PRIVATE_MESSAGE = new EventType("PRIVATE_MESSAGE");
/**
 * Someone disconnects from the server
 * @since 1.0.0
 */
EventType.QUIT = new EventType("QUIT");
/**
 * A channel ban is removed
 * @since 1.0.0
 */
EventType.CHANNEL_BAN_REMOVED = new EventType("CHANNEL_BAN_REMOVED");
/**
 * A channel key is removed
 * @since 1.0.0
 */
EventType.CHANNEL_KEY_REMOVED = new EventType("CHANNEL_KEY_REMOVED");
/**
 * Channel information is received
 * @since 1.0.0
 */
EventType.CHANNEL_INFO = new EventType("CHANNEL_INFO");
/**
 * A channel limit is removed
 * @since 1.0.0
 */
EventType.CHANNEL_LIMIT_REMOVED = new EventType("CHANNEL_LIMIT_REMOVED");
/**
 * A channel is made invitation only
 * @since 1.0.0
 */
EventType.INVITE_ONLY_REMOVED = new EventType("INVITE_ONLY_REMOVED");
/**
 * A channel removes moderation mode
 * @since 1.0.0
 */
EventType.MODERATED_REMOVED = new EventType("MODERATED_REMOVED");
/**
 * A channel removes no-external-messages mode
 * @since 1.0.0
 */
EventType.NO_EXTERNAL_MESSAGES_REMOVED = new EventType("NO_EXTERNAL_MESSAGES_REMOVED");
/**
 * A channel is no longer private
 * @since 1.0.0
 */
EventType.PRIVATE_REMOVED = new EventType("PRIVATE_REMOVED");
/**
 * A channel is no longer secret
 * @since 1.0.0
 */
EventType.SECRET_REMOVED = new EventType("SECRET_REMOVED");
/**
 * A channel removes topic protection
 * @since 1.0.0
 */
EventType.TOPIC_PROTECTION_REMOVED = new EventType("TOPIC_PROTECTION_REMOVED");
/**
 * When we receives a server response line
 * @since 1.0.0
 */
EventType.SERVER_RESPONSE = new EventType("SERVER_RESPONSE");
/**
 * A channel bans someone
 * @since 1.0.0
 */
EventType.SET_CHANNEL_BAN = new EventType("SET_CHANNEL_BAN");
/**
 * A channel sets a channel key
 * @since 1.0.0
 */
EventType.SET_CHANNEL_KEY = new EventType("SET_CHANNEL_KEY");
/**
 * A channel limit is set
 * @since 1.0.0
 */
EventType.SET_CHANNEL_LIMIT = new EventType("SET_CHANNEL_LIMIT");
/**
 * A channel is made invitation only
 * @since 1.0.0
 */
EventType.SET_INVITE_ONLY = new EventType("SET_INVITE_ONLY");
/**
 * A channel is made moderater only
 * @since 1.0.0
 */
EventType.SET_MODERATED = new EventType("SET_MODERATED");
/**
 * A channel no longer accepts external messages
 * @since 1.0.0
 */
EventType.SET_NO_EXTERNAL_MESSAGES = new EventType("SET_NO_EXTERNAL_MESSAGES");
/**
 * A channel is made private
 * @since 1.0.0
 */
EventType.SET_PRIVATE = new EventType("SET_PRIVATE");
/**
 * A channel is made secret
 * @since 1.0.0
 */
EventType.SET_SECRET = new EventType("SET_SECRET");
/**
 * A channel sets topic protection
 * @since 1.0.0
 */
EventType.SET_TOPIC_PROTECTION = new EventType("SET_TOPIC_PROTECTION");
/**
 * A CTCP TIME command is received
 * @since 1.0.0
 */
EventType.TIME = new EventType("TIME");
/**
 * A channel's topic is received
 * @since 1.0.0
 */
EventType.TOPIC = new EventType("TOPIC");
/**
 * An unknown line is received
 * @since 1.0.0
 */
EventType.UNKNOWN = new EventType("UNKNOWN");
/**
 * A channel's user-list is received
 * @since 1.0.0
 */
EventType.USER_LIST = new EventType("USER_LIST");
/**
 * A user mode is set
 * @since 1.0.0
 */
EventType.USER_MODE = new EventType("USER_MODE");
/**
 * A CTCP TIME commmand is received
 * @since 1.0.0
 */
EventType.VERSION = new EventType("VERSION");
/**
 * Someone is given voice
 * @since 1.0.0
 */
EventType.VOICE = new EventType("VOICE");

EventType.restrict();
delete EventType.restrict;
