/*global Packages: false, PircBotWrapper: false, Util: false, Event: false, EventType: false, HazelnutPlugin: false, Tokenizer: false */
/**
 * Creates a Hazlenut framework instance with the given config. By default, the Hazelnut project
 * creates an instance of this for plugins to use.
 *
 * @class
 * The Hazelnut framework is designed to provide useful methods for managing plugins,
 * receving events, performing certain IRC actions, and dealing "authority" (who has
 * the privilege to do "x"). It is designed to be simple, but still flexible.
 *
 * @version 1.0.0
 * @author AMcBain
 * @constructor
 * @param {Object} config The configuration object for Hazelnut
 * @throws an error if no config object is given
 */
function HazelnutFramework(config) {
	var $this = this; // reference to outer object

	if(!(config instanceof Object)) {
		throw "HazlenutFramework#constructor: HazlenutFramework requires a config map (object)";
	}
	config.channels = (config.channels)? config.channels.split(",") : [];
	config.keys     =     (config.keys)? config.keys.split(",")     : [];
	config.auth     =     (config.auth)? config.auth.split(",")     : [];
	config.authcode = (config.authcode)? config.authcode.split(",") : [];
	config.plugins  =  (config.plugins)? config.plugins.split(",")  : [];

	// Event/Plugin maps
	var pluginFamily = {};
	var plugins      = {};
	var events       = {};

	/**
	 * The configuration object of this framework.
	 * <br><br>
	 * <i>Note</i>: Any changes are not saved back to disk (if the config object
	 * was loaded from a file). Plugins which need writeable storage should consider
	 * {@link Util.writeObject}.
	 *
	 * @readonly
	 * @type Object
	 */
	this.config = null;
	this.__defineGetter__("config", function() { return Util.unmodifiableObject(config); });

	/**
	 * A map of the various settings the IRC server reported on successful connection.
	 *
	 * @readonly
	 * @type Object
	 */
	var serverInfo = {};
	this.__defineGetter__("serverInfo", function() { return Util.unmodifiableObject(serverInfo); });


	// PircBot instance
	var pb = new Packages.org.jibble.pircbot.PircBot(new PircBotWrapper());
	pb.setEventHandler(function(type, args) {
		var event = Util.unmodifiableObject(new Event(type, args));

		if(event.type === EventType.SERVER_RESPONSE && event.args.code === 5) {
			var tokenizer = new Tokenizer(event.args.value, " ");
			while(tokenizer.hasNext()) {
				var token = tokenizer.next();

				if(token.charAt(0) === ":") {
					break;
				}

				var eq = token.indexOf("=");
				if(eq !== -1) {
					serverInfo[token.substring(0, eq)] = token.substring(eq + 1);
				} else {
					serverInfo[token] = "";
				}
			}
		}

		var handlers = events[type];

		for(var i in handlers) {
			try {
				handlers[i].onEvent($this, event);
			} catch(e) {
				print("Plugin error caught in handler '" + handlers[i].name+ "' (UID " + handlers[i].uid + ") of the family '" + handlers[i].family + "':");
				print(e);
			}
		}

		if(args.value === config.prefix + "quit") {
			try {
				pb.quitServer("bye");
			} catch(e) {
				print(e);
			}
		}
	});

	if(config.logging === "all") {
		pb.setVerbose(true);
	}
	

	/**
	 * Causes Hazelnut to connect to IRC based on the config information.
	 *
	 * @throws an error if no name is specified in the config
	 * @throws an error if no server is specified in the config
	 * @throws an error if no prefix is specified in the config
	 */
	this.connect = function() {
		if(!config.name)   { throw "HazlenutFramework.connect: Cannot connect to IRC, no bot name specified. Check config."; }
		if(!config.server) { throw "HazlenutFramework.connect: Cannot connect to IRC, no server specified. Check config."; }
		if(!config.prefix) { throw "HazlenutFramework.connect: No prefix specified. Check config."; }

		// Set the bot's nick, and login, and "real name."
		pb.setName(config.name);
		pb.setLogin((config.login)? config.login : config.name);
		pb.setVersion((config.version)? config.version : "See http://hazelnut.asmcbain.net for the latest source");

		// Hey, you've got to have a sense of humor.
		pb.setFinger((config.finger)? config.finger : "I'm a bot. How could you possibly get any enjoyment out of poking me?");

		// Make sure that if our nick is in use, we can still connect.
		pb.setAutoNickChange(true);

		// Other settings
		if(config.password) {
			this.setPassword(config.password);
		}

		pb.connect(config.server);

		// Join any specified channels.
		for(var i = 0; i < config.channels.length; i++) {
			pb.joinChannel(config.channels[i], config.keys[i] || "");
		}
	};


	/**
	 * Sends a message to the specified channel or user.
	 *
	 * @param {String} target The recipient chanel or user.
	 * @param {String} message The message to be sent.
	 */
	this.sendMessage = function(target, message) {
		pb.sendMessage(target, message);
	};

	/**
	 * Sends an action to the specified channel or user.
	 *
	 * @param {String} target The recipient chanel or user.
	 * @param {String} action The action to be sent.
	 */
	this.sendAction = function(target, action) {
		pb.sendAction(target, action);
	};

	/**
	 * Sends a notice to the specified channel or user.
	 *
	 * @param {String} target The recipient chanel or user.
	 * @param {String} notice The notice to be sent.
	 */
	this.sendNotice = function(target, notice) {
		pb.sendNotice(target, notice);
	};

	/**
	 * Sends a message to a channel or a private message to a user, breaking
	 * it up into multiple messages if it is too long to fit on one line. A
	 * typical application of the prefix would be to include another user's
	 * name on each line to alert them (rather than just on the first line).
	 *
	 * @param {String} target The recipient channel or user.
	 * @param {String} message The message to send.
	 * @param {String} [prefix] An optional item with which to prefix each line.
	 */
	this.sendLongMessage = function(target, message, prefix) {
		target = Util.toJsString(target);
		prefix = (typeof prefix === "undefined")? "" : Util.toJsString(prefix);

		var lines = Util.breakUpMessageByIRCLineLength(Util.toJsString(target), Util.toJsString(message), (target.charAt(0) === "#" || target.charAt(0) === "&"), prefix.length, this);
		for(var i = 0; i < lines.length; i++) {
			if(lines[i].charAt(0) === " ") {
				lines[i] = lines[i].substring(1);
			}
			pb.sendMessage(target, prefix + lines[i]);
		}
	};

	/**
	 * Sends a notice to a channel or a notice to a user, breaking it up
	 * into multiple notices if it is too long to fit on one line. A
	 * typical application of the prefix would be to include another user's
	 * name on each line to alert them (rather than just on the first line).
	 *
	 * @param {String} target The recipient channel or user.
	 * @param {String} notice The notice to send.
	 * @param {String} [prefix] An optional item with which to prefix each line.
	 */
	this.sendLongNotice = function(target, notice, prefix) {
		target = Util.toJsString(target);
		prefix = (typeof prefix === "undefined")? "" : Util.toJsString(prefix);

		var lines = Util.breakUpMessageByIRCLineLength(target, Util.toJsString(notice), (target.charAt(0) === "#" || target.charAt(0) === "&"), prefix.length, this);
		for(var i = 0; i < lines.length; i++) {
			if(lines[i].charAt(0) === " ") {
				lines[i] = lines[i].substring(1);
			}
			pb.sendNotice(target, prefix + lines[i]);
		}
	};

	/**
	 * Sends a raw IRC line to the server; use wisely.
	 *
	 * @param {String} line The raw IRC line.
	 */
	this.sendRawLine = function(line) {
		if(!line) { return; }
		if((line = line + "") !== "") {
			pb.sendRawLineViaQueue(line);
		}
	};

	/**
	 * Registers the given handler for the specified event. If the handler is already
	 * registered for that event, it will not be readded.
	 *
	 * @param {EventType} event The name of the event for which this handler is to be registered.
	 * @param {HazelnutPlugin} handler The handler to be registered.
	 * @throws an error if the given handler is missing a uid (unique ID) property
	 * @throws an error if the given handler does not have a family [name] property
	 * @throws an error if the given handler does not have a name property
	 */
	this.addEventHandler = function(event, handler) {
		if(!(event instanceof EventType)) {
			throw "HazelnutFramework.addEventHandler: event must be an instanceof EventType";
		}
		if(handler.constructor === HazelnutPlugin) {
			if(!handler.uid)    { throw "HazelnutFramework.addEventHandler: handler must have a uid";    }
			if(!handler.family) { throw "HazelnutFramework.addEventHandler: handler must have a family"; }
			if(!handler.name)   { throw "HazelnutFramework.addEventHandler: handler must have a name";   }

			if(!events[event] || !events[event][handler.uid]) {

				var name = (handler.name + "").toLowerCase();
				var family = (handler.family + "").toLowerCase();
				if(!pluginFamily[family]) {
					pluginFamily[family] = [];
				}

				family = pluginFamily[family];
				for(var i = 0; i < family.length; i++) {
					if(name === (family[i].name + "").toLowerCase()) {
						throw "HazelnutFramework.addEventHandler: there's already a handler with that name registered for the " + event + " event";
					}
				}
				family.push(handler);

				if(!plugins[handler.uid]) {
					plugins[handler.uid] = [];
				}
				plugins[handler.uid].push(event);

				if(!events[event]) {
					events[event] = {};
				}
				events[event][handler.uid] = handler;
			}
		} else {
			throw "HazelnutFramework.addEventHandler: Handler must extend HazelnutPlugin";
		}
	};

	/**
	 * Wraps the given plugin in an object described by the HazelnutPluginWrapper class.
	 *
	 * @private
	 * @param {HazelnutPlugin} plugin the plugin to be wrapped
	 */
	function wrapPlugin(plugin) {

		// First thing that came to mind to copy the "i" variable like I needed (probably not most elegent) ...
		function wrapFunction(wrapper, i) {
			wrapper[i] = function() {
				plugin[i].apply(plugin, arguments);
			};
		}
		function wrapProperty(wrapper, i) {
			wrapper.__defineGetter__(i, function() { return plugin[i]; });
			wrapper.__defineSetter__(i, function(value) {
		        	plugin[i] = value;
		        });
		}

		var wrapper = {};
		wrapper.family = plugin.family;
		wrapper.name = plugin.name;

		for(var i in plugin) {
			if(i !== "onEvent" && i !== "onMessage" && i !== "onUnload" && i.charAt(0) !== "_") {
				if(plugin[i] instanceof Function) {
					wrapFunction(wrapper, i);
				} else {
					wrapProperty(wrapper, i);
				}
			}
		}

		return plugin;
	}

	/**
	 * Returns an array of the handlers registered for the given event, if any.
	 *
	 * @returns a list of handlers
	 * @type Array
	 */
	this.getHandlersByEvent = function(event) {
		var plugins = [];
		var handlers = events[event];

		for(var i in handlers) {
			plugins.push(wrapPlugin(handlers[i]));
		}

		return plugins;
	};

	/**
	 * Returns an array of the handlers in the given family, if any.
	 *
	 * @returns a list of handlers
	 * @type Array
	 */
	this.getHandlersByFamily = function(family) {
		var plugins = [];
		family = (family + "").toLowerCase();

		if(!pluginFamily[family]) {
			return [];
		}

		family = pluginFamily[family];
		for(var i = 0; i < family.length; i++) {
			plugins.push(wrapPlugin(family[i]));
		}

		return plugins;
	};

	/**
	 * Returns handler with the given name or null if none.
	 *
	 * @returns a handler
	 * @type Object
	 */
	this.getHandlerByName = function(family, name) {
		name = (name + "").toLowerCase();
		family = (family + "").toLowerCase();

		if(!pluginFamily[family]) {
			return null;
		}

		family = pluginFamily[family];
		for(var i = 0; i < family.length; i++) {
			if((family[i].name + "") === name) {
				return wrapPlugin(family[i]);
			}
		}

		return null;
	};

	/**
	 * Removes the specified handler from the given event. If the
	 * hander is not registered for that event, nothing is removed.
	 *
	 * @param {EventType} event The event for which to remove the specified handler.
	 * @param {HazelnutPlugin} handler The handler to remove.
	 * @throws an error if the given handler is not a {@link HazelnutPlugin} instance
	 */
	this.removeEventHandler = function(event, handler) {
		if(handler instanceof HazelnutPlugin) {
			var handlers = events[event];
			delete handlers[handler.uid];
		} else {
			throw "HazelnutFramework#removeHandlerByEvent: Only objects extending HazelnutPlugin may be removed";
		}
	};

	/**
	 * Removes the specified family of handlers. If no family is found,
	 * nothing is removed.
	 *
	 * @param {String} family the family of handlers to be removed.
	 */
	this.removeHandlerFamily = function(family) {
		family = pluginFamily[family];
		if(family) {
			for(var i = 0; i < family.length; i++) {
				var plugin = pluginFamily[i];
				var regEvents = plugins[plugin.uid];
				for(var j = 0; j < regEvents; j++) {
					delete events[regEvents[j]][plugin.uid];
				}
				delete plugins[plugin.uid];
				pluginFamily[i].onUnload();
			}
			delete pluginFamily[family];
		}
	};

	this.toString = function() {
		return "[object HazelnutFramework]";
	};

}
