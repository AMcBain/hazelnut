/*global java: false, HazelnutFramework: false */
/**
 * Creates a Channel object with the given name, and optionally the given users.
 *
 * @class
 * Represents an IRC channel. All channels are tied to a framework instance.
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 * @param {HazelnutFramework} framework The framework with which this User is associated.
 * @param {String} name The name of the channel
 * @param {String} [topic] The topic of the channel
 * @param {String} [modes] The modes set on the channel
 * @param {Array} [users] The users in this channel
 * @throws an error if the supplied framework object is not an instanceof {@link HazelnutFramework}
 * @throws an error if no channel name is provided
 * @throws an error if the supplied users are not an array
 */
function Channel(framework, name, topic, modes, users) {
	if(!(framework instanceof HazelnutFramework)) { throw "User#constructor: framework must be an instance of HazelnutFramework"; }
	if(!name) { throw "Channel#constructor: channel must have a name"; }
	if(typeof users === "undefined") {
		users = [];
	}
	if(!(users instanceof Array)) { throw "Channel#constructor: list users must be an array"; }

	/**
	 * Name of this channel.
	 *
	 * @readonly
	 * @type String
	 */
	this.name = null;
	this.__defineGetter__("name", function() { return name; });

	/**
	 * Topic of this channel.
	 *
	 * @type String
	 */
	this.topic = null;
	topic = topic || "";
	this.__defineGetter__("topic", function() { return topic; });
	this.__defineSetter__("topic", function(value) {
		topic = value + "";
		return value;
	});

	/**
	 * Modes set on this channel. If set to a string starting with a "+", it will add the
	 * given modes. Conversely, it will remove any modes if set to a string starting with "-".
	 *
	 * @type String
	 */
	this.modes = null;
	modes = modes || "";
	this.__defineGetter__("modes", function() { return modes; });
	this.__defineSetter__("modes", function(value) {
		var i;
		if((value + "").charAt(0) === "+") {
			value = value + "";
			for(i = 1; i < value.length;  i++) {
				if(modes.indexOf(value.charAt(i)) === -1) {
					modes += value.charAt(i);
				}
			}
		} else if((value + "").charAt(0) === "-") {
			value = value + "";
			for(i = 1; i < value.length;  i++) {
				modes.replace(value.charAt(i), "");
			}
		} else {
			modes = value + "";
		}
		return value;
	});

	/**
	 * List of users in this channel.
	 *
	 * @type Array
	 */
	this.users = null;
	this.__defineGetter__("users", function() { return users; });
	this.__defineSetter__("users", function(value) {
		if(value instanceof Array) {
			users = value;
		}
		return value;
	});

	this.toString = function() {
		return "[object Channel]";
	};

}
