/*global java */
/**
 * Creates a default Timer object.
 *
 * @class
 * Timer is a simplified timer object that makes it easier to handle timed tasks without
 * importing any Java classes directly. It offers simple ways to stop, start, and change
 * the function called when the timer is fired. The timer fires as close as possible to
 * the set interval.
 *
 * @author A.McBain
 * @since 1.0.0
 * @constructor
 */
function Timer() {
	var $this = this;

	var task = null;
	var ticking = false;
	var interval = 1;
	var fireOnce = false;
	var fixedDelay = true;

	/**
	 * Creates and schedules a new task to call the onTimerFired function of the outer timer.
	 * The old task is canceled to prevent duplicate events.
	 *
	 * @since 1.0.0
	 * @private
	 */
	function scheduleNewTask() {
		task.cancel();
		task = new java.util.TimerTask({
			run: function() {
				if($this.onTimerFired instanceof Function) {
					$this.onTimerFired();
				}
				ticking = fireOnce;
			}
		});

		if(fireOnce) {
			Timer.timer.schedule(task, interval * 1000);
		} else if(fixedDelay) {
			Timer.timer.schedule(task, interval * 1000, interval * 1000);
		} else {
			Timer.timer.scheduleAtFixedRate(task, interval * 1000, interval * 1000);
		}
	}

	/**
	 * <code>true</code> if this timer will fire once when started, <code>false</code>
	 * if it will fire repeated times. The effect of setting this property on an active
	 * timer is undefined.
	 *
	 * @since 1.0.0
	 * @default false
	 * @type boolean
	 */
	this.fireOnce = false;
	this.__defineGetter__("fireOnce", function() {
		return fireOnce;
	});
	this.__defineSetter__("fireOnce", function(value) {
		if(typeof value === "boolean" || value instanceof Boolean) {
			if(ticking) {
				// This behavior could be changed at a later date.
				// Don't rely on it, those who peeked at the source!
				task.cancel();
				ticking = false;
			}
			fireOnce = value;
		}
		return value;
	});

	/**
	 * The method used to manage how the Timer schedules when it fires events.
	 * It is not a good idea to change this value while the timer is running,
	 * as it has to stop the timer then restart it. This setting will have
	 * no effect if {@link #fireOnce} is set to <code>true</code>. 
	 * <br><br>
	 * If set to <code>true</code>, the timer will used <i>fixed-delay</i>
	 * execution. In fixed-delay execution, the Timer will schedule the
	 * next event after the execution of the previous event. Therefore
	 * events which take a long time to return may delay subsequent events.
	 * <br><br>
	 * If set to <code>false</code>, the timer will use <i>fixed-rate</i>
	 * execution. In fixed-rate execution, if an event takes a long time
	 * to return, the next few future events may be executed after a
	 * shorter delay to "catch up." This is more useful for items which
	 * need to perform a fixed number of items in a given time-span or
	 * need to execute at a precise given time (on the hour, for example).
	 *
	 * @since 1.0.0
	 * @default true
	 * @type boolean
	 */
	this.fixedDelay = true;
	this.__defineGetter__("fixedDelay", function() {
		return fixedDelay;
	});
	this.__defineSetter__("fixedDelay", function(value) {
		if(typeof value === "boolean" || value instanceof Boolean) {
			if(value !== fixedDelay) {
				fixedDelay = value;
				scheduleNewTask();
			}
		}
		return value;
	});

	/**
	 * The interval of the timer in seconds; the amount of time between each call to {@link #onTimerFired}.
	 *
	 * @since 1.0.0
	 * @default 1
	 * @type number
	 */
	this.interval = 1;
	this.__defineGetter__("interval", function() {
		return interval;
	});
	this.__defineSetter__("interval", function(value) {
		if(!isNaN(Number(value))) {
			interval = Number(value);
			scheduleNewTask();
		}
		return value;
	});

	/**
	 * <code>true</code> if the timer is currently active, <code>false</code> otherwise.
	 *
	 * @since 1.0.0
	 * @default false
	 * @type boolean
	 */
	this.ticking = false;
	this.__defineGetter__("ticking", function() {
		return ticking;
	});
	this.__defineSetter__("ticking", function(value) {
		if(typeof value === "boolean" || value instanceof Boolean) {
			if(value) {
				if(value !== ticking) {
					scheduleNewTask();
				}
			} else if(task !== null) {
				task.cancel();
			}
			ticking = value;
		}
		return value;
	});

	/**
	 * The function called when the timer fires (the timer's interval is up).
	 *
	 * @since 1.0.0
	 * @default undefined
	 * @type Function
	 */
	this.onTimerFired = undefined;

	this.toString = function() {
		return "[object Timer]";
	};

}
Timer.timer = new java.util.Timer(true); // daemon timer
