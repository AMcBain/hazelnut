/**
 * Creates a Tokenizer with the given input and delimiter and optionally a given starting position.
 *
 * @class
 * The Tokenizer class breaks up input into tokens based on a given delimiter
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 * @param {String} input The input to be tokenized.
 * @param {String} delimiter The value by which to delimit the input.
 * @param {Number} [position] The starting position of the tokenizer.
 * @throws an error if no input is provided
 * @throws an error if no delimiter is provided
 */
function Tokenizer(input, delimiter, position) {

	if(!(input instanceof String || typeof input === "string")) {
		if(input !== undefined && input !== null) {
			print("Tokenizer#constructor: warning >> input not a string, using toString value");
			input = input.toString();
		} else {
			throw "Tokenizer#constructor: input cannot be null or undefined!";
		}
	}

	if(!(delimiter instanceof String || typeof delimiter === "string")) {
		if(delimiter !== undefined && delimiter !== null) {
			print("Tokenizer#constructor: warning >> delimiter not a string, using toString value");
			delimiter = delimiter.toString();
		} else {
			throw "Tokenizer#constructor: delimiter cannot be null or undefined";
		}
	}

	if(input.substring(input.length - delimiter.length) === delimiter) {
		input += delimiter;
	}

	position = (!isNaN(Number(position)))? Number(position) : 0;
	if(input.indexOf(delimiter) === -1) {
		position = input.length;
	}

	/**
	 * Whether this tokenizer has another token it can return.
	 *
	 * @returns <code>true</code> if it can return another token, <code>false</code> otherwise.
	 * @type Boolean
	 */
	this.hasNext = function() {
		return position < input.length;
	};

	/**
	 * Gets the next token and advances the tokenizer. If there is no more input,
	 * this function will throw an exception. Use {@link #hasNext} to determine
	 * if there are more tokens.
	 *
	 * @returns the next token
	 * @type String
	 */
	this.next = function() {

		if(position >= input.length) {
			throw "Tokenizer.next: input exhausted";
		}

		var index = input.indexOf(delimiter, position);
		if(index === -1) {
			index = input.length;
		}

		var result = input.substring(position, index);
		position = index + 1;
		return result;
	};

	/**
	 * Resets the position of the tokenizer.
	 */
	this.reset = function() {
		position = false;
	};

	this.toString = function() {
		return "[object Tokenizer]";
	};

}
