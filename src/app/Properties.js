/*global java: false, Util: false */
/**
 * A namespace to handle reading in and/or saving out properties files.
 *
 * @author A.McBain
 * @since 1.0.0
 * @namespace
 */
var Properties = {

	/**
	 * Reads in the file at the given path and returns a map of its properties.
	 *
	 * @since 1.0.0
	 * @public
	 * @param {String} path The path to the file.
	 * @param {String} lowercase Whether to make all property keys lowercase.
	 * @returns A map of the keys to values specified by the properties file 
	 * @type Object
	 */
	read: function(path, lowercase) {
		var props = java.util.Properties();
		props.load(new java.io.FileInputStream(new java.io.File(path)));
		var map = {};

		var itr = props.entrySet().iterator();
		var entry = null;
		while(Util.isTrue(itr.hasNext())) {
			entry = itr.next();
			if(lowercase) {
				map[Util.toJsString(entry.getKey()).toLowerCase()] = Util.toJsString(entry.getValue());
			} else {
				map[Util.toJsString(entry.getKey())] = Util.toJsString(entry.getValue());
			}
		}

		return map;
	},

	toString: function() {
		return "[namespace Properties]";
	}

};
