/**
 * Constructs a default HazelnutPlugin.
 *
 * @class
 * The base plugin class to be "extended" by all other plugins.
 * A simple HazelnutPlugin.call(this); as the first line in a plugin
 * will suffice. Plugins should override the functions specified by
 * this class to provide whatever functionality they wish to have.
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 */
var HazelnutPlugin = (function() {
	function refSeq(name) {
		if(!refSeq[name]) {
			refSeq[name] = 0;
		}
		return ++refSeq[name];
	}
	var uid = 0;

	return function() {
		var refNum = refSeq(this.constructor.name);
		this.uid = ++uid;

		/**
		 * The family this to which this plugin belongs. Families are used
		 * to group plugins together and allow different plugins to have
		 * the same name. Multiple plugins defined in the same file which
		 * work together may want to have the same family. No two plugins
		 * in the same family may have the same name (this is only enforced
		 * within the HazelnutFramework itself).
		 *
		 * @type String
		 */
		this.family = "";

		/**
		 * The name of this plugin. This is used in conjunction with the
		 * family to identify this plugin.
		 *
		 * @type String
		 */
		this.name = "";


		/**
		 * Returns a string array of command names this plugin represents. The returned names should
		 * match the actual commands matched against incoming IRC data where possible. Some plugins
		 * may not represent any public facing commands, and should just return an empty array. Non-
		 * array values returned should be ignored by any caller.
		 *
		 * @returns an array of command names
		 * @type Array
		 */
		this.listCommands = function() {
			return [];
		};

		/**
		 * Returns help information for a command returned from {@link #listCommands}.
		 *
		 * @param {String} command The command for which to return help information.
		 * @returns help information
		 * @type String
		 */
		this.getCommandHelp = function(command) {
		
		};

		/**
		 * A method for handling IRC events for which this plugins is
		 * registered.
		 *
		 * @param {HazelnutFramework} framework The HazelnutFramework instance that generated this event.
		 * @param {Event} event The event object generated for this IRC event.
		 */
		this.onEvent = function(framework, event) {
		
		};

		/**
		 * A method for receiving data from other plugins. This is usually not
		 * called directly but through the {@link HazelnutPluginWrapper#sendMessage}
		 * method. Only one parameter for "data" is listed, however, callers
		 * can list more and receivers send more. Plugins should check the
		 * calling plugin and only process data given to them by plugins from
		 * which they are expecting data.
		 *
		 * @param {HazelnutPluginWrapper} plugin The plugin sending the data.
		 * @param {Object} data data to be send to this plugin.
		 */
		this.onMessage = function(plugin, data) {
		
		};

		/**
		 * Called when a plugin is unloaded from the given framework. This can be
		 * called either when the plugin is no longer registered for any events, or
		 * when the calling framework is shutting down.
		 *
		 * @param {HazelnutFramework} framework the framework unloading this plugin.
		 */
		this.onUnload = function(framework) {
		
		};

		this.toString = function() {
			return "[object " + this.constructor.name + "#" + refNum + "]";
		};
	};

}());

/**
 * HazelnutPluginWrappers cannot be constructed directly.
 *
 * @class
 * This is the class returned by any calls to the {@link HazelnutFramework}
 * which would return HazelnutPlugins. This wrapper is used to hide certain
 * things on plugins from other plugins and force them to interact by a more
 * standard means.<br><br>
 * HazelnutPluginWrappers will always contain the following defined properties
 * and functions, however any properties and functions defined by the wrapped
 * plugin (which are not prefixed by an underscore) will be properly forwarded.
 * <br><br>
 * Usually only plugins within the same file (and/or possibly the same family)
 * may need more access than is provided by this wrapper, and they should keep
 * a reference around to the original instance instead of using the
 * HazelnutFramework functions.
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 */
function HazelnutPluginWrapper() {

	/**
	 * The family this to which this plugin belongs. Families are used
	 * to group plugins together and allow different plugins to have
	 * the same name. Multiple plugins defined in the same file which
	 * work together may want to have the same family. No two plugins
	 * in the same family may have the same name (this is only enforced
	 * within the HazelnutFramework itself).
	 *
	 * @type String
	 */
	this.family = "";

	/**
	 * The name of this plugin. This is used in conjunction with the
	 * family to identify this plugin.
	 *
	 * @type String
	 */
	this.name = "";

	/**
	 * Returns an array of command names this plugin handles. The returned names should
	 * match the actual commands matched against incoming IRC data where possible. Some plugins
	 * may not represent any public facing commands, and should just return an empty array.
	 * Non-array values returned should be ignored by any caller.
	 *
	 * @returns an array of command names
	 * @type Array
	 */
	this.listCommands = function() {
	
	};

	/**
	 * Returns help information for a command returned from {@link #listCommands}.
	 *
	 * @param {String} command The command for which to return help information.
	 * @returns help information
	 * @type String
	 */
	this.getCommandHelp = function(command) {
	
	};

	/**
	 * A method for sending data to the plugin this object wraps. All
	 * arguments passed to this function are passed a long, however only
	 * the variable data is shown here (for documentation purposes).
	 *
	 * @param {HazelnutPluginWrapper} plugin The plugin sending the data.
	 * @param {Object} data data to be send to this plugin.
	 */
	this.sendMessage = function(plugin, data) {

	};

	throw "HazelnutPluginWrapper#constructor: method not supported";

}
