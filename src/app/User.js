/*global HazelnutFramework: false, java: false, SynchronousEventTask: false, EventType: false, Packages: false */
/**
 * Creates a UserStatus object with an optional prefix to help indicate default state.
 *
 * @class
 * Represents a User's status in a given channel.
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 * @param {String} [prefix] The initial prefix
 */
function UserStatus(prefix) {

	/**
	 * Op status of this user.
	 *
	 * @public
	 * @type Boolean
	 */
	var op = false;
	this.__defineGetter__("op", function() { return op; });
	this.__defineSetter__("op", function(value) { op = !!value; return op; });

	/**
	 * Half-Op status of this user.
	 *
	 * @public
	 * @type Boolean
	 */
	var hop = false;
	this.__defineGetter__("hop", function() { return hop; });
	this.__defineSetter__("op", function(value) { hop = !!value; return hop; });

	/**
	 * Voice status of this user.
	 *
	 * @public
	 * @type Boolean
	 */
	var voice = false;
	this.__defineGetter__("voice", function() { return voice; });
	this.__defineSetter__("voice", function(value) { voice = !!value; return voice; });

	/**
	 * Channel owner status of this user.
	 *
	 * @public
	 * @type Boolean
	 */
	var chowner = false;
	this.__defineGetter__("chowner", function() { return chowner; });
	this.__defineSetter__("chowner", function(value) { chowner = !!value; return chowner; });

	/**
	 * Protected status of this user. The author of this library is not sure what this
	 * status protects one from.
	 *
	 * @public
	 * @type Boolean
	 */
	var protect = false;
	this.__defineGetter__("protected", function() { return protect; });
	this.__defineSetter__("protected", function(value) { protect = !!value; return protect; });

	this.toString = function() {
		return "[object UserStatus]";
	};

	if(prefix === "@") { op = true;      } else
	if(prefix === "%") { hop = true;     } else
	if(prefix === "+") { voice = true;   } else
	if(prefix === "!") { chowner = true; } else
	if(prefix === "~") { chowner = true; } else
	if(prefix === "&") { protect = true; }

}

/**
 * Creates a default Channel object, or one with the given default variables.
 *
 * @class
 * Represents a User on IRC. All User instances are tied to a framework instance.
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 * @param {HazelnutFramework} framework The framework with which this User is associated.
 * @param {String} username The name of the user
 * @param {String} login The login-string of the user
 * @param {String} hostname The hostname of the user
 * @param {String} [prefix] The initial prefix of the user, if applicable
 * @param {String} [channel] The channel in which the prefix applies
 * @throws an error if the supplied framework object is not an instanceof {@link HazelnutFramework}
 */
function User(framework, username, login, hostname, prefix, channel) {
	if(!(framework instanceof HazelnutFramework)) {
		throw "User#constructor: framework must be an instance of HazelnutFramework";
	}


	var authtest = framework.config.authtest;
	if(authtest.charAt(0) === "/" && authtest.lastIndexOf("/") !== 0) {
		var test = authtest.substring(1, authtest.lastIndexOf("/"));
		var flags = authtest.substring(authtest.lastIndexOf("/") + 1);
		if(flags === "" || flags === "g" || flags === "i" || flags === "gi") {
			authtest = new RegExp(test, flags);
		}
	}
	var authcode = framework.config.authcode;

	function createIdentifiedTask(name) {
		var task = new SynchronousEventTask(framework, "identifiedstatusupdaterfor" + name, EventType.SERVER_RESPONSE);
		/** @ignore */
		task.onEvent = function(framework, event) {
			var found = false;
			for(var i = 0; i < authcode.length && !found; i++) {
				found = (Number(authcode[i]) === event.args.code);
			}

			// Only handles the first one of the given code ...
			// Should be fixed to handle multiple lines, stopping only when finding the end code?
			if(found) {
				event.handle();
				if(authtest instanceof RegExp) {
					return authtest.test(event.args.value);
				} else if(event.args.value.indexOf(authtest) !== -1) {
					return true;
				}
			}
			return false;
		};
		return task;
	}

	var system = java.lang.System;
	var identifiedTimestamp = 0;
	var identifiedLock = new java.util.concurrent.Semaphore(1);
	var identifiedTask = null;

	function createAwayTask(name) {
		var task = new SynchronousEventTask(framework, "awaystatusupdaterfor" + name, EventType.SERVER_RESPONSE);
		/** @ignore */
		task.onEvent = function(framework, event) {
			if(event.args.code === Packages.org.jibble.pircbot.PircBot.RPL_WHOREPLY) {
				event.handle();
				// RFC 1459 says this reply is as follows:
				// <channel> <user> <host> <server> <nick> <H|G>[*][@|+] :<hopcount> <real name>
				var tokens = event.args.value.substring(0, event.args.value.lastIndexOf(" :")).split(" ");
				return (tokens[tokens.length - 1].charAt(0) === "G");
			}
		};
		return task;
	}

	var awayTimestamp = 0;
	var awayLock = new java.util.concurrent.Semaphore(1);
	var awayTask = null;


	/**
	 * Username of this user.
	 *
	 * @public
	 * @type String
	 */
	var name = username || "";
	this.__defineGetter__("name", function() { return name; });
	this.__defineSetter__("name", function(value) {
		name = value;

		// Don't want to replace them yet if they're in use
		identifiedLock.acquire();
		identifiedTask = createIdentifiedTask();
		identifiedLock.release();

		awayLock.acquire();
		awayTask = createAwayTask();
		awayLock.release();
	});
	identifiedTask = createIdentifiedTask(name); // damn jslint!
	awayTask = createAwayTask(name);
	

	/**
	 * Login-string of this user.
	 *
	 * @type String
	 */
	this.login = login || "";

	/**
	 * Hostname of this user, typically assigned by the server. Alternatively, this may also
	 * be a "cloak" on some servers which follow pattern usually related to group which
	 * bestowed the cloak.
	 *
	 * @type String
	 */
	this.hostname = hostname || "";

	/**
	 * Whether this user is identified with network services.
	 *
	 * @public
	 * @default false
	 * @type Boolean
	 */
	var identified = false;
	this.__defineGetter__("identified", function() {
		identifiedLock.acquire();

		var currentTimestamp = system.currentTimeMillis();
		if(currentTimestamp - identifiedTimestamp > 15000) {
			framework.sendRawLine("WHOIS " + name);
			identified = identifiedTask.run();
			identifiedTimestamp = system.currentTimeMillis();
		}

		identifiedLock.release();
		return identified;
	});
	this.__defineSetter__("identified", function(value) {
		identifiedLock.acquire();
		identified = !!value;
		identifiedTimestamp = system.currentTimeMillis();
		identifiedLock.release();
		return value;
	});

	/**
	 * Whether this user is identified with network services.
	 *
	 * @public
	 * @default false
	 * @type Boolean
	 */
	var away = false;
	this.__defineGetter__("away", function() {
		awayLock.acquire();

		var currentTimestamp = system.currentTimeMillis();
		if(currentTimestamp - awayTimestamp > 15000) {
			framework.sendRawLine("WHO " + name);
			away = awayTask.run();
			awayTimestamp = system.currentTimeMillis();
		}

		awayLock.release();
		return away;
	});
	this.__defineSetter__("away", function(value) {
		awayLock.acquire();
		away = !!value;
		awayTimestamp = system.currentTimeMillis();
		awayLock.release();
		return value;
	});

	/**
	 * A map of the channels of which the user is a member. Each channel entry is mapped to
	 * an appropriate {@link UserStatus} instance.
	 *
	 * @type UserStatus
	 */
	var channels = {};
	this.__defineGetter__("channels", function() { return channels; });
	this.__defineSetter__("channels", function(value) {
		if(value instanceof Object) {
			channels = value;
		}
		return value;
	});
	if(prefix && channel) {
		this.channels[channel] = new UserStatus(prefix);
	}

	this.toString = function() {
		return "[object User]";
	};

}
