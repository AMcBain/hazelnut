/**
 * Creates a default State object. 
 *
 * @class
 * A class for storing variables with the ability to restore changes at a later time.
 * A simple usage of this class would be for a form, where all values for that form
 * are stored in a state intance. The state instance can be saved before the form is
 * shown to the user, and if the user cancels the form, the state can be rolled back.
 * Similarly, it could be used to make temporary changes to variables, then later
 * roll them back when finished. There are many uses for this class, just use your
 * imagination where appropriate.
 *
 * @author A.McBain, 2009
 * @since 1.0.0
 * @constructor
 */
function State() {
	var stack = [];

	/**
	 * Saves the current state.
	 */
	this.save = function() {
		var state = {};
		for(var property in this) {
			if(this.hasOwnProperty(property)) {
				if(typeof this[property] === "object" && this[property].copy instanceof Function) {
					state[property] = this[property].copy();
				} else {
					state[property] = this[property];
				}
			}
		}
		stack.push(state);
	};

	/**
	 * Restores the previous state.
	 */
	this.restore = function() {
		var state = stack.pop();
		for(var property in state) {
			if(state.hasOwnProperty(property)) {
				this[property] = state[property];
			}
		}
	};

	this.toString = function() {
		return "[object State]";
	};

}

/**
 * Sets the given property to the supplied value and returns the previous entry.
 *
 * @param {String} variable The property being set.
 * @param {Object} value The new value.
 * @returns the old value of the given property
 * @type Object
 */
State.prototype.set = function(variable, value) {
	var previous = this[variable];
	this[variable] = value;
	return previous;
};

/**
 * Gets the given property, and optionally returns the given default value if no property is found with the given name.
 *
 * @param {String} variable The property being gotten.
 * @param {Object} [defaultValue] The value to be returned if no property with the given name is found.
 * @returns the value of the given property
 * @type Object
 */
State.prototype.get = function(variable, defaultValue) {
	return (this.hasOwnProperty(variable))? this[variable] : defaultValue;
};
