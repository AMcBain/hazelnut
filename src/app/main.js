/*global IO: false, HazelnutFramework: false, Properties: false, java: false, EventType: false, HazelnutPlugin: false */
load("app/io.js");

IO.include("app/Properties.js");
IO.include("app/lib/json2.js");
IO.include("app/State.js");
IO.include("app/Util.js");
IO.include("app/Timer.js");
IO.include("app/Event.js");
IO.include("app/EventType.js");
IO.include("app/Tokenizer.js");
IO.include("app/HazelnutPlugin.js");
IO.include("app/SynchronousEventTask.js");
IO.include("app/PircbotWrapper.js");
IO.include("app/HazelnutFramework.js");

IO.include("app/User.js");


// Help
print("");
print("*NIX    - press ^C to force the program to end");
print("Windows - press ^C, then type 'y' to force end");
print("IRC     - use the quit command to close nicely\n");

// Create a new framework instance.
var Hazelnut = new HazelnutFramework(Properties.read("config.txt", true));

// Load any plugins before joining IRC.
if(Hazelnut.config.plugins.length) {
	for(var i = 0; i < Hazelnut.config.plugins.length; i++) {
		try {
			IO.include(IO.PLUGIN_FOLDER + Hazelnut.config.plugins[i] + ".js", true);
		} catch(e) {
			print("'" + Hazelnut.config.plugins[i] + "' failed to load with this error:");
			print(e);
		}
	}
}

// Connecting to the server sometimes fails.
try {
	Hazelnut.connect();
} catch(e) {
	print(e);
	print("Program terminated.");
}
