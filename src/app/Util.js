/*global State: false, HazelnutFramework: false, java: false */
/**
 * A collection of useful functions to handle various things or convert Java objects to JS equivalents.
 *
 * @author A.McBain, 2009
 * @since 1.0.0
 * @namespace
 */
var Util = {

	/**
	 * Checks to see if a Java object is a Java boolean with the value "true",
	 * and returns a JavaScript boolean literal.Only guaranteed to work
	 * correctly for Java Boolean objects.
	 *
	 * @author A.McBain
	 * @since 1.0.0
	 * @param {java.lang.Boolean} value The object to be checked.
	 * @returns a JavaScript boolean literal.
	 * @type boolean
	 */
	isTrue: function(value) {
		return (value + "" === "true");
	},

	/**
	 * Checks to see if a Java object represents the JavaScript value
	 * of undefined and returns a JavaScript boolean literal.Only
	 * guaranteed to work correctly for Java undefined objects.
	 *
	 * @author A.McBain
	 * @since 1.0.0
	 * @param {Object} value The object to be checked.
	 * @returns a JavaScript boolean literal.
	 * @type boolean
	 */
	isUndefined: function(value) {
		return (value + "" === "undefined");
	},

	/**
	 * Checks to see if the first string starts with the second string.
	 *
	 * @author A.McBain
	 * @since 1.0.0
	 * @param {string} value The string to be checked against.
	 * @param {string} value The string for which to be checked.
	 * @returns a string
	 * @type string
	 */
	startsWith: function(value, start) {
		value = value + "";
		return value.substring(0, start.length) === start;

	},

	/**
	 * Removes white-space characters from the beginning and end of the given value.
	 *
	 * @author A.McBain
	 * @since 1.0.0
	 * @param {string} value The string to be trimmed.
	 * @returns a string
	 * @type string
	 */
	trim: function(value) {
		return (value + "").replace(/^\s+/, "").replace(/\s+$/, "");

	},

	/**
	 * Converts a Java String-Object to a JavaScript string literal.
	 *
	 * @author A.McBain
	 * @since 1.0.0
	 * @param {java.lang.String} value The object to be converted.
	 * @returns a JavaScript string literal.
	 * @type string
	 */
	toJsString: function(value) {
		return (value + "");
	},

	/**
	 * Finds the target of a message; if no target is found, null is returned.
	 * <br><br>
	 * In IRC it is not uncommon for a person to direct a message at another
	 * user via a format similar to "nick: message". Bots are also sometimes
	 * addressed via a prefix character, such as "~message", however the
	 * target will only be resolved for messages aimed at Hazelnut.
	 *
	 * @author A.McBain
	 * @since 1.0.0
	 * @param {string} message The message from which to find the target.
	 * @returns the target of the message.
	 */
	findTarget: function(message) {
		var index = message.indexOf(":", 1);
		if(index === -1) {
			index = message.IndexOf(",", 1);
		}
		return (index === -1)? null : message.substring(0, index);
	},

	/**
	 * Removes the target of a message; if no target is found, the message is returned as-is.
	 * For more details, see {@link #findTarget}.
	 *
	 * @author A.McBain
	 * @since 1.0.0
	 * @param {string} message The message from which to have the target removed.
	 * @returns the message sans the target.
	 */
	removeTarget: function(message) {
		var index = message.indexOf(":", 1);
		if(index === -1) {
			index = message.IndexOf(",", 1);
		}
		return (index === -1)? message : message.substring(index + 1);
	},

	/**
	 * Makes the passed object unmodifiable by wrapping it in another object which prevents writes
	 * to properties on the original object. Note that this does not prevent code from defining new
	 * methods or properties on the wrapper, changing properties on the original object, or function
	 * calls from mutating the original object's state. All properties and functions from the original
	 * object are copied to the wrapper.
	 * <br><br>
	 * A special property can be defined on objects that do not wish to be wrapped. If __unwrap__ is
	 * <code>true</code>, this function will simply return the original object.
	 *
	 * @param {Object} object The object to be wrapped
	 * @returns an immutable wrapper object
	 * @type Object
	 */
	unmodifiableObject: function(object) {
		if(object && object.__unwrap__) {
			return object;
		}

		// First thing that came to mind to copy the "i" variable like I needed (probably not most elegent) ...
		function wrapFunction(wrapper, i) {
			wrapper[i] = function() {
				return object[i].apply(object, arguments);
			};
		}
		function wrapArrayIndex(wrapper, i, j) {
			var obj = Util.unmodifiableObject(object[i][j]);
			wrapper.__defineGetter__(wrapper[i], function() { return obj; });
		}
		function wrapProperty(wrapper, i) {
			wrapper.__defineGetter__(i, function() { return object[i]; });
		}
		function wrapObject(wrapper, i) {
			var obj = Util.unmodifiableObject(object[i]);
			wrapper.__defineGetter__(i, function() { return obj; });
			
		}

		var wrapper = {};
		wrapper.constructor = object.constructor;

		for(var i in object) {
			if(object[i] instanceof Function) {
				wrapFunction(wrapper, i);
			} else if(object[i] instanceof Array) {

				wrapper[i] = object[i].slice(0);
				for(var j = 0; j < wrapper[i].length; j++) {
					wrapArrayIndex(wrapper, i, j);
				}
			} else if(object[i] instanceof Object) {
				wrapObject(wrapper, i);
			} else {
				wrapProperty(wrapper, i);
			}
		}

		return wrapper;
	}

};

/**
 * Splits an IRC line up that would be too long otherwise. Takes into account the length of
 * the target (be it a channel or user). If a HazelnutFramework instance is provided, IRC
 * information from its server will be used instead of the stored approximate values.
 * 
 * @function
 * @param {String} target The target of this message (channel, or user)
 * @param {String} message The message to be broken up
 * @param {Boolean} [channel] Whether the given target is a channel (<code>true</code>) or a user (<code>false</code>)
 * @param {Number} [buffer] Amount by which to shorten the IRC line length used in calculations
 * @param {HazelnutFramework} [framework] Framework instance from which to get IRC server information
 * @throws an error if a buffer is provided and it is not a number
 * @throws an error if the given buffer is greater than the defined protocol length
 */
Util.breakUpMessageByIRCLineLength = (function() {
	var s = new State();

	// The total allowed nick length.
	s.NICK_LENGTH = 16;
	// The total allowed channel length.
	s.CHANNEL_LENGTH = 30;
	// Total length for an IRC protocol (minus \r\n line endings)
	s.PROTOCOL_LENGTH = 510;
	// Arbitrary chosen allowed length for login/user-strings.
	s.LOGIN_LENGTH = 10;
	// Total allowed length for hostnames (believed to be accurate).
	s.HOSTNAME_LENGTH = 63;
	// Total length for a command. Longest I found was 7, 1 char buffer.
	s.COMMAND_LENGTH = 8;
	// Extra bits of protocol. 2 spaces, 1 : character. (editor's note: 2 + 1 apparently = 4)
	s.PROTOCOL_ADJUST_LENGTH = 4; // (editor's note: 9 should be considered if this is found to be too little, original was 4)

	// Get the approximate maximum size a message can be, accounting for protocol length.
	function getIRCReceivingMessageLengthInBytes(chanlen, nicklen) {
		return s.PROTOCOL_LENGTH - s.LOGIN_LENGTH - s.HOSTNAME_LENGTH - nicklen - chanlen - s.COMMAND_LENGTH - s.PROTOCOL_ADJUST_LENGTH;
	}

	// Get the approximate max size using the following channel length.
	function getChannelIRCReceivingMessageLengthInBytes(channel) {
		return getIRCReceivingMessageLengthInBytes(channel.length, s.NICK_LENGTH);
	}

	// Get the approximate max size using the following nick length.
	function getUserIRCReceivingMessageLengthInBytes(user) {
		return getIRCReceivingMessageLengthInBytes(s.CHANNEL_LENGTH, user.length);
	}

	// Breaks up a message into multiple lines based on the current (estimated) line length.
	return function(target, message, channel, buffer, framework) {
		s.save();

		if(typeof buffer !== "undefined") {
			if(isNaN(Number(buffer))) {
				throw "Util.breakUpMessageByIRCLineLength: buffer must be a number";
			}
			buffer = Number(buffer);
			if(buffer < s.PROTOCOL_LENGTH) {
				s.PROTOCOL_ADJUST_LENGTH += buffer;
			} else {
				throw "Util.breakUpMessageByIRCLineLength: buffer must be less than " + s.PROTOCOL_LENGTH;
			}
		}

		if(framework instanceof HazelnutFramework) {
			if(framework.serverInfo.NICKLEN) { s.NICK_LENGTH = Number(framework.serverInfo.NICKLEN);       }
			if(framework.serverInfo.CHANLEN) { s.CHANNEL_LENGTH = Number(framework.serverInfo.CHANNELLEN); }
			if(framework.serverInfo.USERLEN) { s.LOGIN_LENGTH = Number(framework.serverInfo.USERLEN);      }
			if(framework.serverInfo.HOSTLEN) { s.HOSTNAME_LENGTH = Number(framework.serverInfo.HOSTLEN);   }
		}

		message = new java.lang.String(message); // yes, we really do want a Java String object!

		// Line byte length.
		var byteLength = (channel)? getChannelIRCReceivingMessageLengthInBytes(target) : getUserIRCReceivingMessageLengthInBytes(target);

		// Store all of our lines into which we've split the message.
		var lines = [];
		// All the "words" in this message.
		var words = message.split("(?=[\\s])");

		// The current StringBuilder.
		var builder = new java.lang.StringBuilder();
		// The current number of bytes in the StringBuilder.
		var byteCount = 0;

		// Loop over all the words.
		for(var i = 0; i < words.length; i++) {
			var bytes = 0;

			try {
				// Get number of bytes in utf-8
				bytes = words[i].getBytes("utf-8").length;
			} catch(e) {
				// Backup plan
				bytes = words[i].getBytes().length;
			}

			// If this would exceed our line byte-limit, start a new line.
			if(byteCount > 0 && byteCount + bytes > byteLength) {
				lines.push(Util.toJsString(builder.toString()));
				builder = new java.lang.StringBuilder();
				byteCount = 0;
				i--; // Back-up one.
			} else if(byteCount === 0 && bytes > byteLength) {
				var word = new java.lang.StringBuilder(words[i]);

				// Add as many as we can.
				for(var j = 0; j < byteLength - builder.length(); j++) {
					builder.append(word.charAt(0));
					word = word.deleteCharAt(0);
				}

				// Add this as the next word, so the remaining bits get added.
				var temp = words.slice(0, i + 1);
				temp.push(word.toString());
				words = temp.concat(words.slice(i + 1));

				byteCount += word.length();
			} else {
				// Add word.
				builder.append(words[i]);
				byteCount += bytes;
			}
		}

		// Add last builder.
		lines.push(Util.toJsString(builder.toString()));

		s.restore();
		return lines;
	};

}());
