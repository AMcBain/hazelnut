/*global EventType: false */
/**
 * Creates an event with the given event type and arguments.
 *
 * @class
 * An event class representing an IRC event or Hazlenut framework event.
 * An event can be "handled" which says that some piece of code which
 * was dispatched the event processed it. This does not mean other code
 * cannot process the event, but it does signify it was handled at least
 * once.
 *
 * @author AMcBain
 * @since 1.0.0
 * @constructor
 * @param {EventType} event The type of the event, one of EventType.EVENT_NAME.
 * @param {Object} args The arguments of the event.
 */
function Event(event, args) {
	if(!(event instanceof EventType)) {
		throw "Event#constructor: event must be an instance of EventType";
	}

	/**
	 * The type of the event, to be compared to an EventType.EVENT_NAME.
	 *
	 * @since 1.0.0
	 * @public
	 * @type Object
	 */
	var type = (EventType[event.name] instanceof EventType)? event : EventType.UNKNOWN;
	this.__defineGetter__("type", function() { return type; });
	this.__defineSetter__("type", function(value) {
		if(EventType[value.name] instanceof EventType) {
			type = value;
		}
		return value;
	});

	/**
	 * The event arguments, these are parsed data from IRC for the given event.
	 * They are all listed below, note that not all events may have data for a
	 * given event argument.
	 * <br><br>
	 * Argument names are case-sensitive. Not all events will have all argumemts,
	 * arguments not available for an event will be undefined.
	 * <table class="summaryTable">
	 *	<caption>Argument Mappings</caption>
	 *	<thead>
	 *		<tr><th>Argument Name</th><th>Value</th></tr>
	 *	</thead>
	 *	<tbody>
	 *		<tr><td>sourceNick</td><td>The name of the person who caused the event</td></tr>
	 *		<tr><td>sourceLogin</td><td>The login-string of the person who caused the event</td></tr>
	 *		<tr><td>sourceHostname</td><td>The hostname of the person who caused the event</td></tr>
	 *		<tr><td>targetNick</td><td>The person to which the event is targeted</td></tr>
	 * 		<tr><td>channel</td><td>The channel in which this event occurred</td></tr>
	 *		<tr><td>value</td><td>The text value of an event. In general it is the "message" of an event</td></tr>
	 *		<tr><td>userCount</td><td>Only used in {@link EventType.CHANNEL_INFO} events, this is the number of users in a channel</td></tr>
	 *		<tr><td>date</td><td>Only used in {@link EventType.TOPIC} events, this is the date the topic was set</td></tr>
	 * 		<tr><td>code</td><td>Only used in {@link EventType.SERVER_RESPONSE} events, this is the code corresponding to the server response type</td></tr>
	 *	</tbody>
	 * </table>
	 *
	 * @type Object
	 */
	this.args = args;

	/**
	 * Whether or not this event has been processed.
	 *
	 * @since 1.0.0
	 * @default false
	 * @type boolean
	 */
	var handled = false;
	this.__defineGetter__("handled", function() {
		return handled;
	});

	/**
	 * Sets this event as having been processed.
	 *
	 * @since 1.0.0
	 */
	this.handle = function() {
		handled = true;
	};

	this.toString = function() {
		return "[object Event]";
	};

}
