/*global java: false, Util: false, HazelnutFramework: false */
/**
 * A collection of items related to helping with input/output, as well as loading other script files.
 *
 * @author AMcBain
 * @since 1.0.0
 * @namespace
 */
function IO() {
	var System = java.lang.System;
	var File = java.io.File;

	/**
	 * The line separator used by the system.
	 *
	 * @since 1.0.0
	 * @public
	 * @readOnly
	 * @string
	 */
	var LINE_BREAK = (System.getProperty("line.separator") || "\n") + "";
	this.__defineGetter__("LINE_BREAK", function() { return LINE_BREAK; });

	/**
	 * The path separator used by the system.
	 * @public
	 * @readOnly
	 * @type string
	 */
	var SEPARATOR = (System.getProperty("path.separator") || ":") + "";
	this.__defineGetter__("SEPARATOR", function() { return SEPARATOR; });

	/**
	 * Forward or backward slash depending on which OS the bot is being run.
	 *
	 * @since 1.0.0
	 * @public
	 * @readOnly
	 * @type string
	 */
	var SLASH = (System.getProperty("file.separator") || "/") + "";
	this.__defineGetter__("SLASH", function() { return SLASH; });

	/**
	 * The path to the folder where the shell or bat file to start the bot was run.
	 *
	 * @since 1.0.0
	 * @readOnly
	 * @public
	 * @type string
	 */
	var PATH = (System.getProperty("user.dir") || "./") + "";
	if(PATH.lastIndexOf(SLASH) !== PATH.length - 1) {
		PATH += SLASH; // add trailing slash
	}
	this.__defineGetter__("PATH", function() { return PATH; });

	/**
	 * The path to the folder where plugins are located.
	 *
	 * @since 1.0.0
	 * @public
	 * @readOnly
	 * @type String
	 */
	var PLUGIN_FOLDER = PATH + "plugins/";
	this.__defineGetter__("PLUGIN_FOLDER", function() { return PLUGIN_FOLDER; });

	/**
	 * The path to the folder where data for plugins is saved.
	 *
	 * @since 1.0.0
	 * @public
	 * @readOnly
	 * @type String
	 */
	var PLUGIN_DATA_FOLDER = PATH + "plugins/data/";
	this.__defineGetter__("PLUGIN_DATA_FOLDER", function() { return PLUGIN_DATA_FOLDER; });

	/**
	 * Includes the given script file into the current runtime. An optional parameter describes whether the path is relative or absolute.
	 * A path is assumed relative if no flag is given.
	 *
	 * @since 1.0.0
	 * @public
	 * @readOnly
	 * @param {string} path The path to the file.
	 * @param {string} [absolute] <code>true</code> if the path is absolute, <code>false</code> if the path is relative to  {@link #path}.
	 * @throws an error if the given file does not exist
	 */
	function include(path, absolute) {
		var error = false;

		if(absolute) {
			if((new File(path)).exists()) {
				load(path);
			} else {
				error = path;
			}
		} else {
			var fpath = PATH + path;
			if((new File(fpath)).exists()) {
				load(fpath);
			} else {
				error = fpath;
			}
		}

		if(error) {
			throw "IO.include: File does not exist! path: '" + error + "'";
		}
	}
	this.__defineGetter__("include", function() { return include; });

	/**
	 * @ignore
	 */
	this.toString = function() {
		return "[namespace IO]";
	};

}
IO = new IO();

/**
 * Gets contents of the specified URL. An optional parameter determines the format of the returned data.
 *
 * @since 1.0.0
 * @param {string} url The url/location to be fetched.
 * @param {boolean} [asLines] <code>true</code> returns the contents as a \n (new line) separated string, <code>false</code> to return the contents as an array of lines.
 * @returns A string or an Array containing the contents of the requested URL.
 * @throws an error if opening a connection to the given URL fails
 */
IO.fetchURL = function(url, asLines) {
	try {

		var link = new java.net.URL(url);
		var conn = link.openConnection();
		var reader = null;

		var line = "";
		var lines = [];
		try {

			reader = new java.io.BufferedReader(new java.io.InputStreamReader(conn.getInputStream()));
			var input = "";

			while((input = reader.readLine()) !== null) {
				if(asLines) {
					lines.push(input + "");
				} else {
					line += input + "\n";
				}
			}

		} catch(ee) {
			throw new Error(ee + "");
		} finally {
			try {
				reader.close();
			} catch(eee) {
				// omnomnom
			}
		}

		return (asLines)? lines : line;
	} catch(e) {
		throw new Error(e + "");
	}
};

/**
 * Reads in the specified file. Optionally returns the contents as a string or an array of lines.
 *
 * @since 1.0.0
 * @param {string} path The path to the file.
 * @param {boolean} asLines <code>true</code> if the output should be returned as an array, <code>false</code> if as a string.
 * @returns <code>true</code> if the output should be returned as an array, <code>false</code> if as a string.
 */
IO.readFile = function(path, asLines) {

	// Store the file temporarily.
	var lines = [];
	var out = "";
	var line = "";

	// Create a new file reader.
	var input = null;

	try {
		input = new java.io.BufferedReader(new java.io.FileReader(new java.io.File(path)));

		// Read in the line from the file. 
		while((line = input.readLine()) !== null) {

			// Add the line to our list.
			if(asLines) {
				lines.push(line);
			} else {
				out += line + "\n";
			}
		}
	} catch (e) {
		throw new Error(e + "");
	} finally {
		try {
			// Try to close the file. It's a good idea and practice.
			input.close();
		} catch(ee) {
			// omnomnom
		}
	}

	return (asLines)? lines : out;
};

/**
 * Writes the given contents to the specified path.
 *
 * @since 1.0.0
 * @param {string} path Path to the file to be written.
 * @param {string} contents The contents of the file to be written.
 * @param {boolean} [append] Whether to append the given contents instead of overwriting.
 */
IO.writeFile = function(path, contents, append) {
	if(!(contents instanceof Array)) {
		contents = [contents];
	}

	// Append file contents?
	var apnd = (append)? true : false; // Make it a real boolean for Java.

	var out = null;
	var error = null;
	try {
		out = new java.io.PrintWriter(new java.io.BufferedWriter(new java.io.FileWriter(path, apnd)));
		//Write summary file. 
		for (var i = 0; i < contents.length; i++) {
			out.println(contents[i]);
		}
	} catch(e) {
		throw new Error(e + "");
	} finally {
		try {
			out.close();
		} catch(ee) {
			// eat
		}
	}
};

/**
 * Checks if the given file exists.
 *
 * @since 1.0.0
 * @param {string} path The path to the file being checked.
 * @returns <code>true</code> if it exists, <code>false</code> otherwise.
 */
IO.fileExists = function(path) {
	return Util.isTrue((new java.io.File(path)).exists());
};

/**
 * Returns an object previously saved via {@link #writeObject}.
 * The first argument is an instance of the Hazelnut Framework, which enables objects which
 * were written relative to a specific instance to be read. This allows plugins to easly get
 * objects back without worrying in what context they were saved (what server, etc). Note that
 * multiple framework instances on the same server may overwrite each other's data.
 *
 * @since 1.0.0
 * @param {HazelnutFramework} framework The framework instance relative to which this object should be read.
 * @param {string} name The name of the object to be read.
 * @returns an instance of the saved object if present; null if otherwise.
 * @type Object
 */
IO.readObject = function(framework, name) {
	if(framework instanceof HazelnutFramework) {
		throw new Error("IO.readObject, first argument must be a valid HazelnutFramework instance");
	}

	var object = null;
	try {
		object = JSON.parse(IO.readFile(IO.PLUGIN_DATA_FOLDER + name + ".js"));
	} catch(e) {
		// We know.
	}

	return object;
};

/**
 * Writes the given object out to a file such that it can be read in via {@link #readObject}.
 * The first argument is an instance of the Hazelnut Framework, which enables objects to be
 * saved relative to different instances, which may be on different servers, etc. Note that
 * this doesn't prevent any issues if multiple instances are on the same framework using the
 * same plugins.
 * <br><br>
 * <b>Note</b>: All primivites will be wrapped in an array for output.
 *
 * @since 1.0.0
 * @param {HazelnutFramework} framework The framework instance relative to which this object should be saved.
 * @param {string} name The name of the object to be saved, to be used when read back in.
 * @param {object} object The object to be saved.
 * @return <code>true</code> if the write was successfull, <code>false</code> otherwise.
 */
IO.writeObject = function(framework, name, object) {
	if(framework instanceof HazelnutFramework) {
		throw new Error("IO.writeObject, first argument must be a valid HazelnutFramework instance");
	}

	// Create the data directory if it exists.
	if(!Util.isTrue((new java.io.File(IO.PLUGIN_DATA_FOLDER)).exists())) {
		(new java.io.File(IO.PLUGIN_DATA_FOLDER)).mkdir();
	}

	// Wrap non-objects, such as primitives.
	if(!(object instanceof Object)) {
		object = [object];
	}

	var error = false;
	try {
		// Create and write the object.
		IO.writeFile(IO.PLUGIN_DATA_FOLDER + name + ".js", JSON.stringify(object));
	} catch(e) {
		error = true;
	}

	return !error;
};

/**
 * Checks if the given object file exists.
 *
 * @since 1.0.0
 * @param {string} name The name of the object file being checked.
 * @returns <code>true</code> if it exists, <code>false</code> otherwise.
 */
IO.objectExists = function(name) {
	return Util.isTrue((new java.io.File(IO.PLUGIN_DATA_FOLDER + name + ".js")).exists());
};
