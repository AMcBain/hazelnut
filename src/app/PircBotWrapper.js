/*global EventType: false, Util: false */
/**
 * Constructs a default PircBotWrapper.
 *
 * @class
 * A class meant to be used as a wrapper over PircBot to capture events 
 *
 * @version 1.0.0
 * @author AMcBain
 * @constructor
 */
function PircBotWrapper() {

	/**
	 * Called when the bot responds to an event from IRC.
	 *
	 * @private
	 * @Function
	 * @param {EventType} type Type of event
	 * @param {Object} event A map of event arguments
	 */
	var processEvent = null;

	/**
	 * Sets the function handler that's called when the bot responds to an event from IRC.
	 *
	 * @since 1.0.0
	 * @param {Function} handler A function to be called later
	 */
	this.setEventHandler = function(handler) {
		if(handler instanceof Function || !handler) {
			processEvent = handler;
		}
	};

	/*this.fireEvent = function(type, args) {
		try {
			// Find all listeners registered for the event (if any)
			var functs = unrestrictedHandlers[type];
			if(functs !== undefined && functs.length > 0) {
				for(var i in functs) {
					// Re-fire the event, pass an empty array if no arguments.
					functs[i](this, type+"", args || []);
				}
			}

			// Has the event been handled?
			var handled = false;

			// Find all listeners registered for the event (if any)
			var functs = restrictedHandlers[type];
			if(functs !== undefined && functs.length > 0) {
				for(var i in functs) {
					if(i !== "unhandled") {
						try {
							// Re-fire the event, pass an empty array if no arguments.
							var value = functs[i](this, type+"", args || []);
							// Has the event been handled?
							if(!handled && value) handled = true;
						} catch(e) {
							print("ERROR: " + e);
						}
					}
				}
			}

			// Call the unhandled event listener.
			if(!handled && functs !== undefined && functs["unhandled"]) functs["unhandled"](this, type+"", args || []);
		} catch(e) {
			if(e.lineNumber) {
				print(e.lineNumber + ":" + e);
			} else {
				print(e);
			}
		}
	};*/


	/**
	 * An internal function to specially handle onMessage events.
	 *
	 * @since 1.0.0
	 * @private
	 * @param {string} type The name of the event.
	 * @param {Array} args The array parameters.
	 * @param {boolean} priv Whether this is a private message or not.
	 */
	/*this.fireMessageEvent = function(type, args, priv) {
		try {
			// Handle only message events.
			if(type == EventType.MESSAGE || type == EventType.PRIVATE_MESSAGE) {

				// Find all listeners registered for the event (if any)
				var functs = unrestrictedHandlers[type];
				if(functs !== undefined && functs.length > 0) {
					for(var i in functs) {
						try {
							// Re-fire the event, pass an empty array if no arguments.
							functs[i](this, type+"", args || []);
						} catch(e) {
							print("ERROR: " + e);
						}
					}
				}

				// Convert our name to the prefix.
				if(args[args.length-1].toLowerCase().indexOf(this.getNick().toLowerCase()+"") === 0) {
					args[args.length-1] = args[args.length-1].replace(new RegExp("^" + this.getNick() + "\\W*\\s*"), obj.prefix).replace(new RegExp("^" + this.getNick().toLowerCase() + "\\W*\\s*"), obj.prefix);
				}

				// Process restricted items.
				if(args[args.length-1].indexOf(prefix) === 0) {

*/						// Remove prefix.
//						args[args.length-1] = args[args.length-1].replace(new RegExp("^\\" + prefix + "\\s*"), "");

					// Catch help/info commands.
//						if(args[args.length-1].match(/^help\s*/) || args[args.length-1].match(/^info\s*/)) {
//							args[args.length-1] = args[args.length-1].replace(/^help\s*/,"").replace(/^info\s*/,"");
/*
						// Send to handler, if there is one.
						var handler = informationHandlers[args[args.length-1].split(/\s+/g)[0]];
						if(handler) {
							try {
								handler(this, type+"", args, priv);
							} catch(e) {
								print("ERROR: " + e);
							}
						} else {
							var handled = false;
							if(unhandledInfoHandler) {
								try {
									handled = unhandledInfoHandler(this, type+"", args, priv);
								} catch(e) {
									print("ERROR: " + e);
								}
							}

							// Do the default action if they don't handle it.
							if(!handled) {
								var cmd = args[args.length-1].substring(0, Math.min(args[args.length-1].length, 20));
								if(args[args.length-1].length > 10) cmd += "...";
								this.sendMessage(args[0], "I could not find any information on '" + cmd + "', " + args[1]);
							}

						}
					} else {
						var handled = false;

						// Find all listeners registered for the event (if any)
						var functs = restrictedHandlers[type];
						if(functs !== undefined && functs.length > 0) {
							for(var i in functs) {
								if(i !== "unhandled") {
									try {
										// Re-fire the event, pass an empty array if no arguments.
										var value = functs[i](this, type+"", args || []);
										// Has the event been handled?
										if(!handled && value) handled = true;
									} catch(e) {
										print("ERROR: " + e);
									}
								}
							}
						}

						// Call the unhandled event listener.
						if(!handled && functs !== undefined && functs["unhandled"]) functs["unhandled"](this, type+"", args || []);
					}
				}				
			}
		} catch(e) {
			if(e.lineNumber) {
				print(e.lineNumber + ":" + e);
			} else {
				print(e);
			}
		}
	}*/


	this.toString = function() {
		return "[object PircBotWrapper]";
	};

	/**
	 * Calls the event handler if set.
	 *
	 * @param {EventType} type Type of event
	 * @param {Object} event A map of event arguments
	 */
	this.onEvent = function(type, event) {
		if(processEvent instanceof Function) {
			processEvent.call(this, type, event);
		}
	};


	this.onAction = function(sourceNick, sourceLogin, sourceHostname, channel, value) {
		this.onEvent(EventType.ACTION, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channel": channel+"", "value": value+""});
	};
	/*this.onChannelInfo = function(channel, userCount, value) {
		this.onEvent(EventType.CHANNEL_INFO, {"channel": channel+"", "userCount": userCount, "value": value+""});
	};*/
	this.onConnect = function() {
		this.onEvent(EventType.CONNECT, {});
	};
	this.onDeop = function(channel, sourceNick, sourceLogin, sourceHostname, channelNick) {
		this.onEvent(EventType.DEOP, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channelNick": channelNick+""});
	};
	this.onDeVoice = function(channel, sourceNick, sourceLogin, sourceHostname, channelNick) {
		this.onEvent(EventType.DE_VOICE, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channelNick": channelNick+""});
	};
	this.onDisconnect = function() {
		this.onEvent(EventType.DISCONNECT, {});
	};
	/*this.onFinger = function(sourceNick, sourceLogin, sourceHostname, channel) {
		super.onFinger(sourceNick, sourceLogin, sourceHostname, channel);
		this.onEvent(EventType.FINGER, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channel": channel+""});
	};*/
	this.onInvite = function(channelNick, sourceNick, sourceLogin, sourceHostname, channel) {
		this.onEvent(EventType.INVITE, {"channelNick": channelNick+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channel": channel+""});
	};
	this.onJoin = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.JOIN, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onKick = function(channel, sourceNick, sourceLogin, sourceHostname, channelNick, value) {
		this.onEvent(EventType.KICK, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channelNick": channelNick+""});
	};
	this.onMessage = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.MESSAGE, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onMode = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.MODE, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onNickChange = function(sourceNick, sourceLogin, sourceHostname, channelNick) {
		this.onEvent(EventType.NICK_CHANGE, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channelNick": channelNick+""});
	};
	this.onNotice = function(sourceNick, sourceLogin, sourceHostname, channel, value) {
		this.onEvent(EventType.NOTICE, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onOp = function(channel, sourceNick, sourceLogin, sourceHostname, channelNick) {
		this.onEvent(EventType.OP, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channelNick": channelNick+""});
	};
	/*this.onPart = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.PART, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};*/
	this.onPart = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.PART, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	/*this.onPing = function(sourceNick, sourceLogin, sourceHostname, channel, value) {
		super.onPing = function(sourceNick, sourceLogin, sourceHostname, channel, value);
		this.onEvent(EventType.PING, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channel": channel+"", "value": value+""});
	};*/
	this.onPrivateMessage = function(sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.PRIVATE_MESSAGE, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onQuit = function(sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.QUIT, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onRemoveChannelBan = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.CHANNEL_BAN_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onRemoveChannelKey = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.CHANNEL_KEY_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onRemoveChannelLimit = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.CHANNEL_LIMIT_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onRemoveInviteOnly = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.INVITE_ONLY_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onRemoveModerated = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.MODERATED_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onRemoveNoExternalMessages = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.NO_EXTERNAL_MESSAGES_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onRemovePrivate = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.PRIVATE_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onRemoveSecret = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.SECRET_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onRemoveTopicProtection = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.TOPIC_PROTECTION_REMOVED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onServerResponse = function(code, value) {
		this.onEvent(EventType.SERVER_RESPONSE, {"code": code, "value": value+""});
	};
	this.onSetChannelBan = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.SET_CHANNEL_BAN, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onSetChannelKey = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.SET_CHANNEL_KEY, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	this.onSetChannelLimit = function(channel, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.SET_CHANNEL_LIMIT, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value});
	};
	this.onSetInviteOnly = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.SET_INVITE_ONLY, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onSetModerated = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.SET_MODERATED, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onSetNoExternalMessages = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.SET_NO_EXTERNAL_MESSAGES, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onSetPrivate = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.SET_PRIVATE, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onSetSecret = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.SET_SECRET, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	this.onSetTopicProtection = function(channel, sourceNick, sourceLogin, sourceHostname) {
		this.onEvent(EventType.SET_TOPIC_PROTECTION, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname});
	};
	/*this.onTime = function(sourceNick, sourceLogin, sourceHostname, channel) {
		super.onTime = function("sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", channel); // Doesn't work unless registered.
		this.onEvent(EventType.TIME, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channel": channel+""});
	};*/
	this.onTopic = function(channel, value, sourceNick, date, changed) {
		if(Util.isTrue(changed)) {
			this.onEvent(EventType.SET_TOPIC, {"channel": channel+"", "value": value+"", "sourceNick": sourceNick+"", "date": new Date(date)});
		} else {
			this.onEvent(EventType.TOPIC, {"channel": channel+"", "value": value+"", "sourceNick": sourceNick+"", "date": new Date(date)});
		}
	};
	this.onUnknown = function(value) {
		this.onEvent(EventType.UNKNOWN, {"value": value+""});
	};
	this.onUserList = function(channel, value) {
		// Warning, this may pass along a Java array, not JS.
		this.onEvent(EventType.USER_LIST, {"channel": channel+"", "value": value});
	};
	this.onUserMode = function(channelNick, sourceNick, sourceLogin, sourceHostname, value) {
		this.onEvent(EventType.USER_MODE, {"channelNick": channelNick+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "value": value+""});
	};
	/*this.onVersion = function(sourceNick, sourceLogin, sourceHostname, channel) {
		super.onVersion(sourceNick, sourceLogin, sourceHostname, channel);
		this.onEvent(EventType.VERSION, {"sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channel": channel+""});
	};*/
	this.onVoice = function(channel, sourceNick, sourceLogin, sourceHostname, channelNick) {
		this.onEvent(EventType.VOICE, {"channel": channel+"", "sourceNick": sourceNick+"", "sourceLogin": sourceLogin+"", "sourceHostname": sourceHostname+"", "channelNick": channelNick+""});
	};

}
