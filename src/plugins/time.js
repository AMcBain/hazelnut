/**********************************************
* Public Domain                               *
* ------------------------------------------- *
* A simple time plugin. It responds to "time" *
* and returns the current time of the machine *
* on which the bot is being run.			  *
**********************************************/

if(!this["time"]) {
	var time = {
		format: function(value) {
			var a = Number(value);
			return (a + (a > 10 && a < 20 ? "th" : a % 10 === 1 ? "st" : a % 10 === 2 ? "nd" : a % 10 === 3 ? "rd" : "th"));
		},
		months: ["January","February","March","April","May","June","July","August","September","October","November","December"]
	};
}

// Remove any other "time" plugins.
Hazelnut.removeHandlerFamily("time");

// Reads off the current time somewhere.
Hazelnut.addEventHandler(EventType.MESSAGE, (function() {

	var plugin = new HazelnutPlugin();
	plugin.family = "time";
	plugin.name = "time";
	plugin.onEvent = function(framework, event) {

		if (event.args.value.charAt(0) !== framework.config.prefix) {
			return;
		}

		var msg = event.args.value.substring(1);
		var channel = event.args.channel;
		var sender = event.args.sourceNick;

		// Only respond to "time" messages.
		if(msg === "time" || msg.substring(0, 5) === "time ") {
			msg = Util.trim(msg.substring(4));
			var date = new Date();

			// Handle bot-local time.
			if(msg.substring(0, 4) === "time") {
				msg = Util.trim(msg.substring(4));
				var hours = date.getHours();
				var minutes = date.getMinutes();
				var post = "";
				if(msg === "12") {
					if(hours > 12) {
						hours = hours - 12;
						post = "pm";
					} else {
						post = "am";
					}
				}
				framework.sendMessage(channel, sender + ", It is now " + hours + ":" + ((minutes < 10)? "0": "") + minutes + ((post)? " " + post : "") + " where I live.");
			} else if(msg === "date") {
				framework.sendMessage(channel, sender + ", Today is " + time.months[date.getMonth()] + " " + time.format(date.getDate()) + ", " + date.getFullYear());
			} else {
				framework.sendMessage(channel, sender + ", It is now " + date);
			}

			return true;
		}
	};

	return plugin;
}()));
