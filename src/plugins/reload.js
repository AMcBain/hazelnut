/**************************************************
* Public Domain                                   *
* ----------------------------------------------- *
* A test plugin to reload specified files. The    *
* filename given is automatically assumed to be   *
* ".js", so the extension should be omitted. In   *
* order for this plugin to work fully (and not    *
* just load a plugin twice) the specified plugin  *
* must have a line to unregister any previous     *
* copies of the plugin.                           *
**************************************************/

Hazelnut.removeHandlerFamily("reload");
Hazelnut.addEventHandler(EventType.MESSAGE, (function() {
	var plugin = new HazelnutPlugin();
	plugin.family = "reload";
	plugin.name = "reload";
	plugin.onEvent = function(framework, event) {

		if (event.args.value.charAt(0) !== framework.config.prefix) {
			return;
		}

		var msg = event.args.value.substring(1);
		var channel = event.args.channel;
		var sender = event.args.sourceNick;

		if(msg.substring(0, 7) === "reload ") {
			try {
				var file = event.args.value.substring(8);
				IO.include(IO.PLUGIN_FOLDER + file + ".js", true);

				framework.sendMessage(channel, sender + ", " + file + " reloaded.");
			} catch(e) {
				framework.sendMessage(channel, sender + ", reloading " + file + " failed; See log.");
				print("'" + Hazelnut.config.plugins[i] + "' failed to load with this error:");
				print(e);
			}
		}
	};

	return plugin;
}()));
